package com.cidenet.api.humantalent.interfaz;

import java.util.List;

import org.javatuples.Pair;

import com.cidenet.api.humantalent.entity.Employee;

/**
 * Interfaz CRUD para la entidad empleado
 * 
 * @author lromero
 *
 */
public interface IEmployeeService {
	/**
	 * Crea un empleado dado en db
	 * 
	 * @param employee Empleado a crear
	 * @return Tupla que contiene si fue borrado exitosamente y un mensaje en caso
	 *         que no
	 */
	public Pair<Boolean, String> createEmployee(Employee employee);

	/**
	 * Actualiza un empleado dado en db
	 * 
	 * @param employee Empleado a actualizar
	 * @return Tupla que contiene si fue borrado exitosamente y un mensaje en caso
	 *         que no
	 */
	public Pair<Boolean, String> updateEmployee(Employee employee);

	/**
	 * Elimina un empleado dado en db
	 * 
	 * @param employee Empleado a borrar
	 * @return Tupla que contiene si fue borrado exitosamente y un mensaje en caso
	 *         que no
	 */
	public Pair<Boolean, String> deleteEmployee(Employee employee);

	/**
	 * Lista todos los empleados de la base de datos
	 * 
	 * @return Lista de empleados
	 */
	public List<Employee> listEmployees();
}
