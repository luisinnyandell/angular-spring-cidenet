package com.cidenet.api.humantalent.interfaz;

import java.util.List;

import org.javatuples.Pair;

import com.cidenet.api.humantalent.entity.Country;

/**
 * Interfaz Para Obtener la entidad pais
 * 
 * @author lromero
 *
 */
public interface ICountryService {
	/**
	 * Crea un pais dado en db
	 * @param country pais a crear
	 * @return Tupla que contiene si fue actualizado exitosamente y un mensaje en caso que no
	 */
	public Pair<Boolean,String> createCountry(Country country);
	/**
	 * Actualiza un pais dado en db
	 * @param country pais a actualizar
	 * @return Tupla que contiene si fue actualizado exitosamente y un mensaje en caso que no
	 */
    public Pair<Boolean, String> updateCountry(Country country);
    /**
     * Elimina un pais dado en db
     * @param country Pais a borrar
     * @return Tupla que contiene si fue borrado exitosamente y un mensaje en caso que no
     */
    public Pair<Boolean, String> deleteCountry(Country country);
	/**
	 * Lista todos los paises de la base de datos
	 * @return Lista de paises
	 */
    public List<Country> listCountries(); 
}	 