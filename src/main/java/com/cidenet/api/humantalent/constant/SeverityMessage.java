package com.cidenet.api.humantalent.constant;


/**
 * Enumera los distintos tipos de severidad en los mensajes.
 * 
 * @author lromero
 * 
 */
public enum SeverityMessage {
    /**
     * Es informativo
     */
    INFORMATIVE("I"),
    /**
     * Es una advertencia
     */
    WARNING("W"),
    /**
     * Es un error
     */
    ERROR("E");
    private final String SeverityMessage;

    private SeverityMessage(String SeverityMessage) {
        this.SeverityMessage = SeverityMessage;
    }

    public String getSeverityMessage() {
        return SeverityMessage;
    }
    
}
