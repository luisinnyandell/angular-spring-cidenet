package com.cidenet.api.humantalent.constant;
/**
 * Enumera los tipos de identificación.
 * 
 * @author lromero
 * 
 */
public enum IdentificationType {
	/**
	 * Cédula de ciudadania
	 */
	_CEDULA_CIUDADANIA("CC","Cédula de Ciudadania"),
	/**
	 * Cédula de extranjería
	 */
	_CEDULA_EXTRANJERIA("CE","Cédula de Extranjería"),
	/**
	 * Pasaporte
	 */
	_PASAPORTE("PP","Pasaporte"),
	/**
	 * Permiso especial
	 */
	_PERMISO_ESPECIAL("PE","Permiso especial");
	
    private final String identificationCode;
    private final String identificationDescription;
    
    
    private IdentificationType(String identificationCode, String identificationDescription) {
        this.identificationCode = identificationCode;
        this.identificationDescription = identificationDescription;
    }

    public String getIdentificationDescription() {
        return identificationDescription;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }
}
