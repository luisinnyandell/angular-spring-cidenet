package com.cidenet.api.humantalent.constant;
/**
 * Enumera los validadores de campos en forma de expresión regular.
 * 
 * @author lromero
 * 
 */
public enum RegExpValidators {
	/**
	 * Expresión regular solo letras de A-Z
	 */
	_ONLY_A_Z("^[a-zA-Z]+$"),
	/**
	 * Expresión regular alfanumericos
	 */
	_ONLY_ALPHANUMBER("^[a-zA-Z\\d]+$"),
	/**
	 * Expresión regular PRIMER_NOMBRE.PRIMER_APELLIDO.ID?@DOMINIO
	 */
	_INTERNAL_MAIL("^[a-zA-Z0-9]+.+[a-zA-Z0-9]+[.*\\d+]*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+).+(\\.[a-zA-Z0-9-])*$");
	
    private final String regExp;    
    
    private RegExpValidators(String regExp) {
        this.regExp = regExp;
    }

    public String getRegExp() {
        return regExp;
    }
}
