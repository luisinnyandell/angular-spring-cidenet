package com.cidenet.api.humantalent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.api.humantalent.entity.Country;

/**
 * Repositorio para entidad Pais
 * 
 * @author lromero
 *
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
	 public Country findByalpha2Code(String alpha2Code);
}
