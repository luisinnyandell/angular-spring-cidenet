package com.cidenet.api.humantalent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.api.humantalent.constant.IdentificationType;
import com.cidenet.api.humantalent.entity.Country;
import com.cidenet.api.humantalent.entity.Employee;

/**
 * Repositorio para entidad Empleado
 * 
 * @author lromero
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	/**
	 * Busca un empleado por su correo electronico
	 * 
	 * @param correo Correo electronico
	 * @return Objeto Empleado encontrados
	 */
	public Employee findByCorreoElectronico(String correo);

	/**
	 * Busca un empleado por su tipo y numero de documento
	 * 
	 * @param tIdent Tipo de identificacion
	 * @param nIdent Numero de identificacion
	 * @return Objeto Empleado encontrados
	 */
	public Employee findByTipoIdentificacionAndNumeroIdentificacion(IdentificationType tIdent, String nIdent);

	/**
	 * Busca un empleado por su pais de empleo
	 * 
	 * @param country Pais con el cual buscar los empleados
	 * @return Objeto Empleado encontrados
	 */
	public Employee findByPaisEmpleo(Country country);
}
