package com.cidenet.api.humantalent.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.javatuples.Pair;

import com.cidenet.api.humantalent.constant.RegExpValidators;
import com.cidenet.api.humantalent.entity.Employee;
/**
 * Clase Validadora de empleados, los constructores pueden ser para empleado anonimo o 
 * un empleado especifico
 * 
 * @author lromero
 *
 */
public class EmployeeValidator {

	private Employee employee;
	/**
	 * Expresión regular compilada para validar nombres
	 */
	private Pattern regNames = Pattern.compile(RegExpValidators._ONLY_A_Z.getRegExp());
	/**
	 * Expresión regular compilada para validar carácteres alfanumericos
	 */
	private Pattern regCedula = Pattern.compile(RegExpValidators._ONLY_ALPHANUMBER.getRegExp());
	/**
	 * Expresión regular compilada para validar Email Interno de la forma
	 * PRIMER_NOMBRE.PRIMER_APELLIDO.ID@DOMINIO
	 */
	private Pattern regCorreo = Pattern.compile(RegExpValidators._INTERNAL_MAIL.getRegExp());

	public EmployeeValidator() {

	}

	public EmployeeValidator(Employee e) {
		this.employee = e;
	}

	/**
	 * Validador de correo electronico del empleado sea valido
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si el correo electronico es valido, falso en caso que no
	 */
	public Boolean emailValidator(Employee e) {
		Matcher matcher = regCorreo.matcher(e.getCorreoElectronico());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validador de cedula del empleado sea alfanumerica
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si la cedula es valida, falso en caso que no
	 */
	public Boolean cedulaValidator(Employee e) {
		Matcher matcher = regCedula.matcher(e.getNumeroIdentificacion());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validador de nombres del empleado sean a-z/A-Z
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si el Primer nombre es valido, falso en caso que no
	 */
	public Boolean primerNombreValidator(Employee e) {
		Matcher matcherPN = regNames.matcher(e.getPrimerNombre());
		if (matcherPN.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validador de nombres del empleado sean a-z/A-Z
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si el segundo nombre es valido, falso en caso que no
	 */
	public Boolean segundoNombreValidator(Employee e) {
		Matcher matcherSN = regNames.matcher(e.getSegundoNombre());
		if (matcherSN.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validador de apellido del empleado sean a-z/A-Z
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si el primer apellido es valido, falso en caso que no
	 */
	public Boolean primerApellidoValidator(Employee e) {
		Matcher matcherPA = regNames.matcher(e.getPrimerApellido());
		if (matcherPA.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validador de apellido del empleado sean a-z/A-Z
	 * 
	 * @param e Objeto Empleado a validar
	 * @return verdadero si el segundo apellido es valido, falso en caso que no
	 */
	public Boolean segundoApellidoValidator(Employee e) {
		Matcher matcherSA = regNames.matcher(e.getSegundoApellido());
		if (matcherSA.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Valida si un empleado cumple con todas las condiciones de ser valido
	 * 
	 * @param e Objeto Empleado a validar
	 * @return Tupla con valor booleano verdadero en caso de ser valido y un mensaje
	 *         que valida el usuario El valor booleano es falso en caso de que algún
	 *         criterio no cumpla y el mensaje dirá cual criterio es invalido
	 */
	public Pair<Boolean, String> isValidEmployee(Employee e) {
		Pair<Boolean, String> valid;
		if (!this.emailValidator(e)) {
			valid = new Pair<Boolean, String>(false, "Email no es valido");
		} else {
			if (!this.cedulaValidator(e)) {
				valid = new Pair<Boolean, String>(false, "Cédula no es valida");
			} else {
				if (!this.primerNombreValidator(e)) {
					valid = new Pair<Boolean, String>(false, "Primer Nombre no es valido");
				} else {
					if (!this.segundoNombreValidator(e)) {
						valid = new Pair<Boolean, String>(false, "Segundo Nombre no es valido");
					} else {
						if (!this.segundoApellidoValidator(e)) {
							valid = new Pair<Boolean, String>(false, "Segundo Apellido no es valido");
						} else {
							if (!this.primerApellidoValidator(e)) {
								valid = new Pair<Boolean, String>(false, "Primer Apellido no es valido");
							} else {
								valid = new Pair<Boolean, String>(true, "Emplado valido");
							}
						}
					}
				}
			}
		}

		return valid;
	}
	
	/**
	 * Valida si el empleado del objeto cumple con todas las condiciones de ser valido
	 *  
	 * @return Tupla con valor booleano verdadero en caso de ser valido y un mensaje
	 *         que valida el usuario El valor booleano es falso en caso de que algún
	 *         criterio no cumpla y el mensaje dirá cual criterio es invalido
	 */
	public Pair<Boolean, String> isValidEmployee() {
		return this.isValidEmployee(this.employee);
	}

}
