package com.cidenet.api.humantalent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class CidenetHumantTalentApiApplication {
	
	@RequestMapping("/")
	public String greet(){
	   return "welcome!";
	}

	public static void main(String[] args) {
		SpringApplication.run(CidenetHumantTalentApiApplication.class, args);
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/country/**").allowedOrigins("*");
				registry.addMapping("/employee/**").allowedOrigins("*");
			}
		};
	}

}
