package com.cidenet.api.humantalent.implement;

import java.util.List;
import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cidenet.api.humantalent.entity.Employee;
import com.cidenet.api.humantalent.interfaz.IEmployeeService;
import com.cidenet.api.humantalent.repository.EmployeeRepository;

/**
 * Implementación del servicio de empleados
 * 
 * @author lromero
 *
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Pair<Boolean, String> createEmployee(Employee employee) {
		Pair<Boolean, String> employeeSavedResponse;
		try {
			Employee e = employeeRepository.findByTipoIdentificacionAndNumeroIdentificacion(
					employee.getTipoIdentificacion(), employee.getNumeroIdentificacion());
			if (e != null) {
				employeeSavedResponse = new Pair<Boolean, String>(false, "Error, el empleado ya está registrado");
				return employeeSavedResponse;
			}

			e = employeeRepository.findByCorreoElectronico(employee.generateCorreoElectronico());
			Integer sec = 1;
			while (e != null) {
				e = employeeRepository.findByCorreoElectronico(employee.reGenerateCorreoElectronico(sec));
				sec++;
			}

			employeeRepository.save(employee);
			employeeSavedResponse = new Pair<Boolean, String>(true, "Empleado Guardado con exito");
			return employeeSavedResponse;
		} catch (Exception e) {
			employeeSavedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return employeeSavedResponse;
		}

	}

	@Override
	public Pair<Boolean, String> updateEmployee(Employee employee) {
		Pair<Boolean, String> employeeSavedResponse;
		try {
			employeeRepository.save(employee);
			employeeSavedResponse = new Pair<Boolean, String>(true, "Empleado actualizado con exito");
			return employeeSavedResponse;
		} catch (Exception e) {
			employeeSavedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return employeeSavedResponse;
		}
	}

	@Override
	public Pair<Boolean, String> deleteEmployee(Employee employee) {
		Pair<Boolean, String> employeeDeletedResponse;
		try {
			Optional<Employee> lemployee = employeeRepository.findById(employee.getId());
			if (!lemployee.isPresent()) {
				employeeDeletedResponse = new Pair<Boolean, String>(false, "No existe el empleado");
				return employeeDeletedResponse;
			}
			employeeRepository.delete(employee);
			employeeDeletedResponse = new Pair<Boolean, String>(true, "Empleado eliminado con exito");
			return employeeDeletedResponse;
		} catch (Exception e) {
			employeeDeletedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return employeeDeletedResponse;
		}
	}

	@Override
	public List<Employee> listEmployees() {
		return employeeRepository.findAll();
	}

}
