package com.cidenet.api.humantalent.implement;

import java.util.List;
import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.api.humantalent.entity.Country;
import com.cidenet.api.humantalent.interfaz.ICountryService;
import com.cidenet.api.humantalent.repository.CountryRepository;

/**
 * Implementación del servicio de pais
 * 
 * @author lromero
 *
 */
@Service
public class CountryServiceImpl implements ICountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Override
	public Pair<Boolean, String> createCountry(Country country) {
		Pair<Boolean, String> countrySavedResponse;
		try {
			Country c = countryRepository.findByalpha2Code(country.getAlpha2Code());
			if (c != null) {
				countrySavedResponse = new Pair<Boolean, String>(false, "Error, el pais ya está registrado");
				return countrySavedResponse;
			}
			countryRepository.save(country);
			countrySavedResponse = new Pair<Boolean, String>(true, "País Guardado con exito");
			return countrySavedResponse;
		} catch (Exception e) {
			countrySavedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return countrySavedResponse;
		}		
	}

	@Override
	public Pair<Boolean, String> updateCountry(Country country) {
		Pair<Boolean, String> countrySavedResponse;
		try {
			countryRepository.save(country);
			countrySavedResponse = new Pair<Boolean, String>(true, "País actualizado con exito");
			return countrySavedResponse;
		} catch (Exception e) {
			countrySavedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return countrySavedResponse;
		}
	}

	@Override
	public Pair<Boolean, String> deleteCountry(Country country) {
		Pair<Boolean, String> countryDeletedResponse;
		try {
			Optional<Country> lc = countryRepository.findById(country.getId());
			if (!lc.isPresent()) {
				countryDeletedResponse = new Pair<Boolean, String>(false, "No existe el país");
				return countryDeletedResponse;
			}
			countryRepository.delete(country);
			countryDeletedResponse = new Pair<Boolean, String>(true, "País eliminado con exito");
			return countryDeletedResponse;
		} catch (Exception e) {
			countryDeletedResponse = new Pair<Boolean, String>(false, "Error" + e.toString());
			return countryDeletedResponse;
		}
	}

	@Override
	public List<Country> listCountries() {
		return countryRepository.findAll();
	}

}
