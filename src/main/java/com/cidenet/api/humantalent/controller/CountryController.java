package com.cidenet.api.humantalent.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.api.humantalent.entity.Country;
import com.cidenet.api.humantalent.entity.Respuesta;
import com.cidenet.api.humantalent.interfaz.ICountryService;

@RestController
@RequestMapping(path = "/country")
public class CountryController {
	@Autowired
	private ICountryService countryDAO;
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseEntity<Respuesta> listUser() {
		Respuesta response;
		try {
			List<Country> e = countryDAO.listCountries();
			response = new Respuesta(true, "Paises listados");
			response.setBodyData(e);
		} catch (Exception e) {
			response = new Respuesta(false, "Error listando" + e);
		}

		return ResponseEntity.ok().body(response); // ;
	}
}
