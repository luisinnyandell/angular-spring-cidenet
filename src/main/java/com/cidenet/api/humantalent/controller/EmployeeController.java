package com.cidenet.api.humantalent.controller;

import java.util.List;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.api.humantalent.entity.Employee;
import com.cidenet.api.humantalent.entity.Respuesta;
import com.cidenet.api.humantalent.interfaz.IEmployeeService;

@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {
	@Autowired
	private IEmployeeService employeeDAO;

	@RequestMapping(value = "/set", method = RequestMethod.POST)
	public ResponseEntity<Respuesta> setUser(@RequestBody Employee u) {
		Pair<Boolean, String> rps = employeeDAO.createEmployee(u);
		Respuesta response = new Respuesta(rps.getValue0(), rps.getValue1());
		return ResponseEntity.ok().body(response); // ;
	}

	@RequestMapping(value = "/put", method = RequestMethod.POST)
	public ResponseEntity<Respuesta> putUser(@RequestBody Employee u) {
		Pair<Boolean, String> rps = employeeDAO.updateEmployee(u);
		Respuesta response = new Respuesta(rps.getValue0(), rps.getValue1());
		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(value = "/del", method = RequestMethod.POST)
	public ResponseEntity<Respuesta> delUser(@RequestBody Employee u) {
		Pair<Boolean, String> rps = employeeDAO.deleteEmployee(u);
		Respuesta response = new Respuesta(rps.getValue0(), rps.getValue1());
		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseEntity<Respuesta> listUser() {
		Respuesta response;
		try {
			List<Employee> e = employeeDAO.listEmployees();
			response = new Respuesta(true, "Empleados listados");
			response.setBodyData(e);
		} catch (Exception e) {
			response = new Respuesta(false, "Error listando" + e);
		}

		return ResponseEntity.ok().body(response); // ;
	}
}
