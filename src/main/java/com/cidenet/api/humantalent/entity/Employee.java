package com.cidenet.api.humantalent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.cidenet.api.humantalent.constant.IdentificationType;

/**
 * Modelo de la entidad Empleado que encapsula los datos como sus nombres,
 * apellidos, país de empleo Identificación y correo electronico
 * 
 * @author lromero
 *
 */

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String primerNombre = "";
	private String segundoNombre = "";
	private String primerApellido = "";
	private String segundoApellido = "";

	@ManyToOne
	@JoinColumn(name = "pais_empleo_id")
	private Country paisEmpleo = null;

	private IdentificationType tipoIdentificacion = null;
	@Column(unique = true)
	private String numeroIdentificacion = "";
	@Column(unique = true)
	private String correoElectronico = "";

	public Employee() {

	}

	public Employee(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido,
			Country paisEmpleo, IdentificationType tipoIdentificacion, String numeroIdentificacion) {
		super();
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.paisEmpleo = paisEmpleo;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.correoElectronico = generateCorreoElectronico();
	}

	/**
	 * Crea un correo electronico con la estructura básica de cidenet
	 * 
	 * @return String con el correo electronico con la estructura dada
	 */
	public String generateCorreoElectronico() {
		this.correoElectronico = (primerNombre.replaceAll("\\s", "") + "." + primerApellido.replaceAll("\\s", "") + "@"
				+ paisEmpleo.getDomain()).toLowerCase();
		return this.correoElectronico;
	}

	/**
	 * Crea un correo electronico con la estructura básica de cidenet más un scuencial
	 * 
	 * @return String con el correo electronico con la estructura dada
	 */
	public String reGenerateCorreoElectronico(Integer secuential) {
		this.correoElectronico = (primerNombre.replaceAll("\\s", "") + "." + primerApellido.replaceAll("\\s", "") + "." + secuential + "@"
				+ paisEmpleo.getDomain()).toLowerCase();
		return this.correoElectronico;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Country getPaisEmpleo() {
		return paisEmpleo;
	}

	public void setPaisEmpleo(Country paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}

	public IdentificationType getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(IdentificationType tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

}
