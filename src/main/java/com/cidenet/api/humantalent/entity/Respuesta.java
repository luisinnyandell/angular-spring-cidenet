package com.cidenet.api.humantalent.entity;

/**
 * Crea una objeto que contendra el tipo de respuesta de una solicitud, y el
 * resultado de la misma
 * 
 * @author lromero
 *
 * @param <T> Clase que llevará el body de la respuesta
 */

public class Respuesta<T> {
	private String success = "";
	private String error = "";
	private String message = "";
	private T bodyData = null;

	/**
	 * Obtiene el cuerpo de una respuesta
	 * 
	 * @return Objeto de la respuesta
	 */
	public T getBodyData() {
		return bodyData;
	}

	/**
	 * Fija el cuerpo de la respuesta
	 * 
	 * @param bodyData Objeto a guardar en la respuesta
	 */
	public void setBodyData(T bodyData) {
		this.bodyData = bodyData;
	}

	public Respuesta() {

	}

	public Respuesta(Boolean success, String message) {
		this.success = success.toString();
		this.error = success ? "false" : "true";
		this.message = message;
	}

	public Respuesta(String success, String error, String message) {
		super();
		this.success = success;
		this.error = error;
		this.message = message;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
