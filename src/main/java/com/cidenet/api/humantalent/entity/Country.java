package com.cidenet.api.humantalent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Modelo de la entidad Paises de empleo según la norma internacional ISO-3166-1
 * 
 * @author lromero
 *
 */
@Entity
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String countryName = "";
	private String officialStateName = "";
	
	@Column(unique = true)
	private String alpha2Code = "";
	
	@Column(unique = true)
	private String alpha3Code = "";
	
	@Column(unique = true)
	private Integer numericCode = 0;
	private String domain = "";

	/*public Country(String countryName, String officialStateName, String alpha2Code, String alpha3Code,
			Integer numericCode, String domain) {
		super();
		this.countryName = countryName;
		this.officialStateName = officialStateName;
		this.alpha2Code = alpha2Code;
		this.alpha3Code = alpha3Code;
		this.numericCode = numericCode;
		this.domain = domain;
	}*/

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getOfficialStateName() {
		return officialStateName;
	}

	public void setOfficialStateName(String officialStateName) {
		this.officialStateName = officialStateName;
	}

	public String getAlpha2Code() {
		return alpha2Code;
	}

	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}

	public String getAlpha3Code() {
		return alpha3Code;
	}

	public void setAlpha3Code(String alpha3Code) {
		this.alpha3Code = alpha3Code;
	}

	public Integer getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(Integer numericCode) {
		this.numericCode = numericCode;
	}

}
