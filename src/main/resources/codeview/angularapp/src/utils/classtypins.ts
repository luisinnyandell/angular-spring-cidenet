
/**
 *
 * Clase que define el modelo país 
 *
 */
export class Country {
	public id: number;
	public numericCode: number;
	public alpha2Code: string;
	public alpha3Code: string;
	public domain: string;
	public officialStateName: string;
	public countryName: string;
}

/**
 *
 * Clase que define el modelo Empleado 
 *
 */
export class Employee {
	public id: number;
	public tipoIdentificacion: string;
	public numeroIdentificacion: string;
	public primerNombre: string;
	public segundoNombre: string;
	public primerApellido: string;
	public segundoApellido: string;
	public correoElectronico: string;		
	public paisEmpleo: Country;
}

/**
 *
 * Clase que define el modelo Usuario 
 *
 */
export class User {
	public username: string;
	public password: string;
}