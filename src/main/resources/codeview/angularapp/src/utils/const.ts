
/**
 *
 * Enumera los tipos de identificación.
 * 
 */
export enum IdentificationType {
	/**
	 * Cédula de ciudadania
	 */
	_CEDULA_CIUDADANIA = "Cédula de Ciudadania",
	/**
	 * Cédula de extranjería
	 */
	_CEDULA_EXTRANJERIA = "Cédula de Extranjería",
	/**
	 * Pasaporte
	 */
	_PASAPORTE = "Pasaporte",
	/**
	 * Permiso especial
	 */
	_PERMISO_ESPECIAL = "Permiso especial"
}