import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { Country, Employee } from '../../../utils/classtypins';
import { IdentificationType }  from '../../../utils/const';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.scss']
})
export class ListEmployeeComponent implements OnInit {

	private actual_id: number = -1;

	public employees: Employee[] = [];
	public countries: Country[] = [];

	public action: string = "";
	public identificationTypes = IdentificationType;
	public employee: Employee = {
		id: -1,
		tipoIdentificacion: "",
		numeroIdentificacion: "",
		primerNombre: "",
		segundoNombre: "",
		primerApellido: "",
		segundoApellido: "",
		correoElectronico: "",
		paisEmpleo: null,
	}

	keys() : Array<string> {
        return Object.keys(this.identificationTypes);
    }


	constructor(private service: ApiService, private router: Router) { }

	ngOnInit(): void {
		let logged = localStorage.getItem("Logged");
		if(logged != null){
			if(logged == "false")
				this.router.navigate(["home"]);
		}

		this.service.getCountry((data) => {
			this.service.closeDialog();
			if(data.hasOwnProperty("error") && data.error == "true") 
				this.service.presentToast("¡Error!", "Algo paso: " + data.message , "error");
			else{				
				this.countries = data.bodyData;
			}
		});

		this.service.getEmployee((data) => {
			this.service.closeDialog();
			if(data.hasOwnProperty("error") && data.error == "true") 
				this.service.presentToast("¡Error!", "Algo paso: " + data.message , "error");
			else{				
				this.employees = data.bodyData;
			}
		});

		console.log(IdentificationType);
	}
	/**
	 *
	 * Reinicia las variables para creación de empleado
	 *
	 */
	toNew(){
		this.action = "save";
		this.employee.id = -1,
		this.employee.tipoIdentificacion = "";
		this.employee.numeroIdentificacion = "";
		this.employee.primerNombre = "";
		this.employee.segundoNombre = "";
		this.employee.primerApellido = "";
		this.employee.segundoApellido = "";
		this.employee.correoElectronico = "";
		this.employee.paisEmpleo = null;
	}

	/**
	 *
	 * Valida y Guarda las variables para creación de empleado
	 *
	 */
	guardar(){

		if(this.employee.primerNombre == "" || this.employee.segundoNombre == ""){ 
			this.service.presentToast("¡Error!", "Los nombres no pueden ser vacío", "error");
			return;
		}
		if(this.employee.primerApellido == "" || this.employee.segundoApellido == "") {
			this.service.presentToast("¡Error!", "Los apellidos no pueden ser vacío", "error");
			return;
		}
		if(this.employee.tipoIdentificacion == "") {
			this.service.presentToast("¡Error!", "Debes seleccionar un tipo de documento", "error");
			return;
		}
		if(this.employee.numeroIdentificacion == "") {
			this.service.presentToast("¡Error!", "La cédula no puede ser vacío", "error");
			return;
		}		

		let regOnlyAlpha = new RegExp("^[a-zA-Z\d]+$");

		if(!regOnlyAlpha.test(this.employee.numeroIdentificacion)) {
			this.service.presentToast("¡Error!", "La cédula solo pueden ser carácteres alfanumericos", "error");
			return;
		}

		if(this.action == "save") this.doGuardar();
		else this.doEditar();
		
	}

	/**
	 *
	 * Envía el objeto empleado actualizado a la base de datos
	 *
	 */
	doEditar(){
		this.employee.id = this.actual_id;

		this.service.putEmployee(this.employee, (data) => {
			this.service.closeDialog();
			if(data.hasOwnProperty("error") && data.error == "true") this.service.presentToast("¡Error!", "Algo paso: " + data.message, "error");
			else{
				this.employee.id = -1,
				this.employee.tipoIdentificacion = "_CEDULA_CIUDADANIA";
				this.employee.numeroIdentificacion = "";
				this.employee.primerNombre = "";
				this.employee.segundoNombre = "";
				this.employee.primerApellido = "";
				this.employee.segundoApellido = "";
				this.employee.correoElectronico = "";
				this.employee.paisEmpleo = null;
				this.service.hideModal("#agregarEmployee");
				this.ngOnInit();
			}
		});
	}

	/**
	 *
	 * Envía el objeto empleado a la base de datos
	 *
	 */
	doGuardar(){
		delete this.employee.id;

		console.log(this.employee);

		this.service.setEmployee(this.employee, (data) => {
			this.service.closeDialog();
			if(data.hasOwnProperty("error") && data.error == "true") this.service.presentToast("¡Error!", "Algo paso: " + data.message, "error");
			else{
				this.employee.id = -1,
				this.employee.tipoIdentificacion = "";
				this.employee.numeroIdentificacion = "";
				this.employee.primerNombre = "";
				this.employee.segundoNombre = "";
				this.employee.primerApellido = "";
				this.employee.segundoApellido = "";
				this.employee.correoElectronico = "";
				this.employee.paisEmpleo = null;
				this.service.hideModal("#agregarEmployee");
				this.ngOnInit();
			}
		});
	}

	/**
	 *
	 * Carga el objeto empleado en el formulario modal
	 *
	 */
	editar(employee: Employee){
		this.actual_id = employee.id;
		this.employee.id = employee.id;
		this.employee.tipoIdentificacion = employee.tipoIdentificacion;
		this.employee.numeroIdentificacion = employee.numeroIdentificacion;
		this.employee.primerNombre = employee.primerNombre;
		this.employee.segundoNombre = employee.segundoNombre;
		this.employee.primerApellido = employee.primerApellido;
		this.employee.segundoApellido = employee.segundoApellido;
		this.employee.correoElectronico = employee.correoElectronico;
		this.employee.paisEmpleo = employee.paisEmpleo;
		this.action = "edit";
		this.service.presentModal("#agregarEmployee");
	}

	/**
	 *
	 * Envía el objeto empleado a eliminar de la base de datos
	 *
	 */
	eliminar(employee){
		console.log(employee)
		let confirmar = (result) => {
	      if (result.isConfirmed) {  
	        this.service.delEmployee(employee, (data) =>{
	          this.service.closeDialog();
	          if(data.hasOwnProperty("error") && data.error == "true") this.service.presentToast("¡Algo paso!", data.message, "error");
	          else this.service.presentToast("¡Hecho!", "Evento eliminado");
	          this.ngOnInit();
	        });            
	      }
	    };

	    let buttons = {
	        showCancelButton: true,
	        cancelButtonText: 'Cancelar',
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: 'Confirmar'
	      }
		this.service.presentAlertConfirm("¡Atención!", "¿Deseas borrar?", buttons, confirmar)
	}

	/**
	 *
	 * Cierra la sesión actual
	 *
	 */
	salir(){
		localStorage.setItem("Logged", "false");
		this.router.navigate(["home"]);
	}

}
