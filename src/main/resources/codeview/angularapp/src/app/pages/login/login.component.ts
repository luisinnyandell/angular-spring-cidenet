import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { User } from '../../../utils/classtypins';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user: User = {
    username: "root",
    password: "123",
  };
  public param: any = {
    alert: "",
    type: "success"
  }

  constructor( private service: ApiService, private router: Router) { }

  ngOnInit(): void {
  	let logged = localStorage.getItem("Logged");
  	if(logged != null){
  		if(logged == "true")
  			this.router.navigate(["list"]);
  	}
  }
  /**
   *
   * Verifica que el usuario y la contraseña sean validos
   * @todo Validar con backend usuario
   *
   */
  login(){
  	if(this.user.username == "" || this.user.password == ""){
  		this.service.presentToast("¡Error!", "Los campos no pueden ser vacíos", "error");
  		return;
  	}else{

  		if(this.user.username == "root")
  			if(this.user.password == "123"){
  				localStorage.setItem("Logged", "true");
  				this.router.navigate(["list"]);
  			}
  			else
  				this.service.presentToast("¡Error!", "La contraseña no coincide", "error");
  		else
  			this.service.presentToast("¡Error!", "No existe usuario", "error");
  	}

  }

}
