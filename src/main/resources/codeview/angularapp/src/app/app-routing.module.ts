import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { ListEmployeeComponent } from './pages/list-employee/list-employee.component';

const routes: Routes = [
		{ path: '', redirectTo:'home', pathMatch: 'full' },
		{ path: 'home', component: LoginComponent },
		{ path: 'list', component: ListEmployeeComponent },
		{ path: '**', pathMatch: 'full', redirectTo: '' }
	];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
