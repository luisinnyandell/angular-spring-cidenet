import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";

import Swal from 'sweetalert2';

declare var $: any;

@Injectable({
    providedIn: 'root'
})
export class ApiService{

    private api = environment.api; 
    private headers = new HttpHeaders();
    private accesstype: any = [];
    msgSended = false;

    constructor(private httpClient:HttpClient){
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/x-www-urlencoded');
        this.headers.append('No-Auth', 'True');
    }
    /**
    * Despliega un modal de bootstrap 
    * @param modalid - Id del modal a presentar
    *
    */
    presentModal(modalid: string){
      $(modalid).modal("show");
    }
    /**
    * Oculta un modal de bootstrap 
    * @param modalid - Id del modal a esconder
    *
    */
    hideModal(modalid){
      $(modalid).modal("hide"); 
    }
    /**
    * Muestra una alerta con Sweetalert2
    *
    * @param title - El titulo a mostrar
    * @param mensaje - El monsaje a mostrar en pantalla
    * @param additionalOpt - Opciones extras de Sweetaler2
    * @param callback - Función de retorno a llamar cuando se termine la alerta
    */
    async presentAlertConfirm(title, mensaje, additionalOpt = null, callback = null ) {

        let options = {
          title: title,
          text: mensaje
        }

        if(additionalOpt != null)
          for(let it in additionalOpt)
            options[ it ] = additionalOpt[it];          

        Swal.fire(options).then(callback);
    }
    /**
    * Muestra una alerta en forma de toast en una esquina de la pantalla
    *
    * @param title - El titulo a mostrar
    * @param mensaje - El monsaje a mostrar en pantalla
    * @param type - Tipo de alerta a mostrar    
    */
    async presentToast( title, mensaje, type = "success") {      
        switch(type){
          case "success":
            Swal.fire(title, mensaje, "success");
          break;
          case "error":
            Swal.fire(title, mensaje, "error");
          break;
          case "warning":
            Swal.fire(title, mensaje, "warning");
          break;
          case "info":
            Swal.fire(title, mensaje, "info");
          break;
          case "question":
            Swal.fire(title, mensaje, "question");
          break;
        }     
    }

    /**
    * Hace una solicitud a un url dada, luego llama la función  de retorno al terminar
    *
    * @param url - Enlace a hacer solicitud
    * @param params - Array con los parametros a enviar con la solicitud
    * @param callBack - Función a llamar cuando se haga la solicitud
    * @param type - Tipo de solicitud a hacer (GET - POST)
    * @param headers - Encabezados de la solicitud 
    *
    */
    async doRequest (url, params, callBack, type , headers: any = {}){

        let load ={
          title: 'Cargando',
          html: 'Por favor, espere...',
          allowOutsideClick: false,
          allowEnterKey: false,
          allowEscapeKey: false,
          willOpen: () => {
            Swal.showLoading();            
          }         
        }

        if( !params.hasOwnProperty("DisableLoad") ) Swal.fire(load);
        if( typeof params == "string") if( !params.includes("DisableLoad")) Swal.fire(load);

        switch( type.toUpperCase() ){
            case 'GET':
                this.httpClient.get(url, headers)
                            .pipe( map( resp => { return resp; } ) )
                            .subscribe(callBack, er => {
                                this.presentToast("Error: ", er, "error");
                               //Swal.close();
                            }, () => {
                             //Swal.close();
                            });                            
            break;

            case 'POST':
                this.httpClient.post(url, params, headers)
                            .pipe( map( resp => { return resp; } ) )
                            .subscribe(callBack, er => {
                                try{
                                    er = JSON.stringify(er);
                                    this.presentToast("Error: ", er, "error");
                                }catch(e){
                                    this.presentToast("Error: ", er, "error");
                                } 
                               //Swal.close();                               
                            }, () => {
                             //Swal.close();
                            });
            break;
        }
    };

    /**
    * Cierra cualquier alerta hecha por sweetalert2
    *
    *
    */
    closeDialog(){
      Swal.close();
    }

    /**
    * Envia un empleado al backend
    * @param userData - el usuario a guardar
    * @param callBack - Función de retorno a llamar al terminar la solicitud
    */
    setEmployee( userData: any, callBack ){        
        this.doRequest(`${this.api}/employee/set`, userData, callBack, 'post');
    }    

    /**
    * Obtiene la lista de empleados del backend
    * @param callBack - Función de retorno a llamar al terminar la solicitud
    */
    getEmployee( callBack ){
        this.doRequest(`${this.api}/employee/list`, {}, callBack, 'post');
    }

    /**
    * Envia un empleado al backend
    * @param userData - el usuario a actualizar
    * @param callBack - Función de retorno a llamar al terminar la solicitud
    */
    putEmployee( userData: any, callBack ){
       this.doRequest(`${this.api}/employee/put`, userData, callBack, 'post');
    }

    /**
    * Envia un empleado al backend
    * @param userData - el usuario a eliminar
    * @param callBack - Función de retorno a llamar al terminar la solicitud
    */
    delEmployee( userData: any, callBack ){
       this.doRequest(`${this.api}/employee/del`, userData, callBack, 'post');
    }

    /**
    * Obtiene la lista de paises del backend
    * @param callBack - Función de retorno a llamar al terminar la solicitud
    */
    getCountry( callBack ){
        this.doRequest(`${this.api}/country/list`, {}, callBack, 'post');
    }

}