# Autor

Luis Eduardo Romero Castro
CC 1082994898
Prueba Técnica full stack Cidenet Angular - Spring Boot

# Como empezar
## Servidor de desarrollo

Para correr el servidor en modo pruebas de angular debes ir a la carpeta `src\main\resources\codeview\angularapp` y en una consola de comandos usar `ng serve` (Requiere tener instalado NodeJS, Angular-Cli y haber ejecutado el comando `npm install`) que hará un servidor de desarrollo. en tu explorador ve a `http://localhost:4200/`. la aplicación se mostrará en modo desarrolo

### Guia de despliegue

El actual proyecto está listo para desplegarse y funcionar bajo el puerto 8080, en el proyecto encontrarás un archivo .bat que te ayudará en lo que requieras, para desplegar un servidor interno abre una consola de comandos en la raíz y escribe `mvnw package` (Requiere tener instalado Java 11).  La  configuración de la base de datos la podrás encontrar en el archivo `appliation.properties` ubicado en `src\main\resources`
ahí, debes colocar los datos de conexión y configurar en modo `create` la linea `spring.jpa.hibernate.ddl-auto` la primera vez que arranques el proyecto, esto constrirá la base de datos con Hibernate, luego cambia el argumento a `none`. la aplicación trae un set de datos ubicado en `src\main\resources` con el nombre `data.sql`.

Para correr el servidor puedes usar  `java -jar target/cidenetHumantTalentAPI-0.0.1-SNAPSHOT.jar` desde la raiz luego de haber usado el comando `package` de Maven, Esto te permitirá acceder a la aplicación en tu navegador con la URL `http://localhost:8080`

Existe un archivo de base de datos en la carpeta raiz, incluye esquema y datos de prueba

En la ubicación `fsnapshots` se encuentra capturas de pantalla mostrando el funcionamiento del proyecto


### Documentación de referencia
A continuación tienes toda la documentación que servirá para este proyecto en general

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.4/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Rest Repositories](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#howto-use-exposing-spring-data-repositories-rest-endpoint)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#boot-features-developing-web-applications)
* [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md)