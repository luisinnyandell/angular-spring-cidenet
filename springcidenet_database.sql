-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 07-04-2021 a las 19:29:58
-- Versión del servidor: 8.0.21
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `springcidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int NOT NULL,
  `alpha2code` varchar(255) DEFAULT NULL,
  `alpha3code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `numeric_code` int DEFAULT NULL,
  `official_state_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_837jcu5oc26d8y6yfyuek3m7l` (`alpha2code`),
  UNIQUE KEY `UK_1yc0rjqnvkobrtvvqjxgbhyiy` (`alpha3code`),
  UNIQUE KEY `UK_ik5yv6a3ayb6i3fl69nuv2cbg` (`numeric_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`id`, `alpha2code`, `alpha3code`, `country_name`, `domain`, `numeric_code`, `official_state_name`) VALUES
(1, 'US', 'USA', 'Estados Unidos de America', 'cidenet.com.us', 840, 'The United States of America'),
(2, 'CO', 'COL', 'Colombia', 'cidenet.com.co', 170, 'Republica de Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int NOT NULL,
  `correo_electronico` varchar(255) DEFAULT NULL,
  `numero_identificacion` varchar(255) DEFAULT NULL,
  `primer_apellido` varchar(255) DEFAULT NULL,
  `primer_nombre` varchar(255) DEFAULT NULL,
  `segundo_apellido` varchar(255) DEFAULT NULL,
  `segundo_nombre` varchar(255) DEFAULT NULL,
  `tipo_identificacion` int DEFAULT NULL,
  `pais_empleo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tb9eyyxj2m3y4kie1gkungccr` (`correo_electronico`),
  UNIQUE KEY `UK_fdcbrv31o2ncytrn0b5r32lp1` (`numero_identificacion`),
  KEY `FK8i7r8mgg56yuclbnvdrbuigg5` (`pais_empleo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `employee`
--

INSERT INTO `employee` (`id`, `correo_electronico`, `numero_identificacion`, `primer_apellido`, `primer_nombre`, `segundo_apellido`, `segundo_nombre`, `tipo_identificacion`, `pais_empleo_id`) VALUES
(0, 'luis.romero@cidenet.com.co', '1082994898', 'Romero', 'Luis', 'Castro', 'Eduardo', 0, 2),
(4, 'luis.romero.1@cidenet.com.co', '1082994899', 'Romero', 'Luis', 'Castro', 'Manuel', 0, 2),
(7, 'walter.romero@cidenet.com.us', '123221121', 'ROMERO', 'WALTER ', 'CASTRO', 'STIVEN', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(8);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK8i7r8mgg56yuclbnvdrbuigg5` FOREIGN KEY (`pais_empleo_id`) REFERENCES `country` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
